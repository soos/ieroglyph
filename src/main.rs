use image::{imageops, Luma, GenericImage, ImageBuffer};
use rand::Rng;

fn main() {
    let imgx = 64*10+63+2;
    let imgy = 64*10+63+2;

    let mut imgbuf: ImageBuffer<Luma<u8>,Vec<u8>> = ImageBuffer::new(imgx, imgy);

    for x in 0..imgx {
        for y in 0..imgy {
            imgbuf.put_pixel(x, y, Luma {data: [255u8]});
        }
    }

    let mut glyphs = [0; 4096];

    for i in 0..4096 {
        glyphs[i] = i;
    }

    let mut rng = rand::thread_rng();

    for index in 0..4096 {
        let random_index = rng.gen_range(0, 4096);
        let tmp = glyphs[random_index];
        glyphs[random_index] = glyphs[index];
        glyphs[index] = tmp;
    }

    for x in 0..64 {
        for y in 0..64 {
            let mut subimg = imageops::crop(&mut imgbuf, (1+x*11) as u32, (1+y*11) as u32, 10, 10);
            let pixel = Luma {data: [0u8]};
            for i in 0..9 {
                subimg.put_pixel(i, 0, pixel);
                subimg.put_pixel(9, i, pixel);
                subimg.put_pixel(9-i, 9, pixel);
                subimg.put_pixel(0, 9-i, pixel);
            }

            subimg.put_pixel(3, 3, pixel);
            subimg.put_pixel(6, 3, pixel);
            subimg.put_pixel(3, 6, pixel);
            subimg.put_pixel(6, 6, pixel);

            let glyph = glyphs[y * 64 + x]; //x | (y << 6);

            if (glyph & (1 << 0)) != 0 {
                subimg.put_pixel(3, 1, pixel);
                subimg.put_pixel(3, 2, pixel);
            }

            if (glyph & (1 << 1)) != 0 {
                subimg.put_pixel(6, 1, pixel);
                subimg.put_pixel(6, 2, pixel);
            }

            if (glyph & (1 << 2)) != 0 {
                subimg.put_pixel(3, 4, pixel);
                subimg.put_pixel(3, 5, pixel);
            }

            if (glyph & (1 << 3)) != 0 {
                subimg.put_pixel(6, 4, pixel);
                subimg.put_pixel(6, 5, pixel);
            }

            if (glyph & (1 << 4)) != 0 {
                subimg.put_pixel(3, 7, pixel);
                subimg.put_pixel(3, 8, pixel);
            }

            if (glyph & (1 << 5)) != 0 {
                subimg.put_pixel(6, 7, pixel);
                subimg.put_pixel(6, 8, pixel);
            }

            if (glyph & (1 << 6)) != 0 {
                subimg.put_pixel(1, 3, pixel);
                subimg.put_pixel(2, 3, pixel);
            }

            if (glyph & (1 << 7)) != 0 {
                subimg.put_pixel(4, 3, pixel);
                subimg.put_pixel(5, 3, pixel);
            }

            if (glyph & (1 << 8)) != 0 {
                subimg.put_pixel(7, 3, pixel);
                subimg.put_pixel(8, 3, pixel);
            }

            if (glyph & (1 << 9)) != 0 {
                subimg.put_pixel(1, 6, pixel);
                subimg.put_pixel(2, 6, pixel);
            }

            if (glyph & (1 << 10)) != 0 {
                subimg.put_pixel(4, 6, pixel);
                subimg.put_pixel(5, 6, pixel);
            }

            if (glyph & (1 << 11)) != 0 {
                subimg.put_pixel(7, 6, pixel);
                subimg.put_pixel(8, 6, pixel);
            }
        }
    }

    imgbuf.save("ieroglyphs.png").unwrap();
}
